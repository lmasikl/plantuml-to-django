import pathlib
from collections import defaultdict


def write_decorator(foo):
    def wrapper(*args):
        lines = foo(*args)
        lines.pop()
        lines = '\n'.join(lines)
        filename = foo.__name__.split('_')[-1]
        filename = f'{filename}.py'
        with open(pathlib.Path.cwd() / filename, 'w') as output_file:
            output_file.write(lines)

    return wrapper


class Reader:
    start_class_indicator = 'class'
    end_class_indicator = '}'
    attribute_divider = ':'
    is_required_indicator = '+'

    def __init__(self, filepath):
        with open(filepath, 'r') as db_file:
            self.file_lines = db_file.readlines()
        
        self.classes = {}
        self.reading_class = None

    def _is_start_of_class(self, line):
        result = False
        if self.start_class_indicator in line:
            result = True

        return result

    def _start_read_class(self, line):
        class_name = line.split()[1].strip()
        self.reading_class = class_name
        self.classes[class_name] = []
    
    def _is_end_of_class(self, line):
        result = False
        if self.reading_class and line.strip() == self.end_class_indicator:
            result = True

        return result
    
    def _stop_read_class(self, line):
        self.reading_class = None
    
    def _read_class_line(self, line):
        field, properties = line.split(self.attribute_divider)
        is_required, name = field.split()
        is_required = is_required == self.is_required_indicator
        field_type, *attributes = properties.split()
        self.classes[self.reading_class].append({
            'name': name,
            'type': field_type,
            'attributies': attributes,
            'is_required': is_required,
        })
        
    def read_classes(self):
        for line in self.file_lines:
            if self._is_start_of_class(line):
                self._start_read_class(line)
                continue

            if self._is_end_of_class(line):
                self._stop_read_class(line)
                continue
            
            if self.reading_class:
                self._read_class_line(line)


class Writer:
    CHAR = 'Char'
    FK = 'FK'
    fields_map = {
        CHAR: 'CharField',
        FK: 'ForeignKey'
    }

    indent = ' ' * 4
    empty_line = ''
    primary_key_indicator = 'id'

    def __init__(self, schema):
        self.schema = schema
        self.field_attributes = {
            self.CHAR: self._char_field_attributies,
            self.FK: self._fk_field_attributies,
        }
    
    def _char_field_attributies(self, attributies):
        assert len(attributies) > 0, attributies
        return f'max_length={attributies[0]}'
    
    def _fk_field_attributies(self, attributies):
        assert len(attributies) > 0, attributies
        return f'{attributies[0]}'
    
    @write_decorator
    def write_models(self):
        lines = [
            'from django.db import models',
            self.empty_line,
            self.empty_line,
        ]

        for class_name, fields in self.schema.items():
            lines.append(f'class {class_name}(models.Model):')
            for field in fields:
                if field['name'] == self.primary_key_indicator:
                    continue

                field_type = field['type']
                properties = field['attributies']
                properties = [self.field_attributes[field_type](properties)]
                if not field['is_required']:
                    properties.append('blank=True, null=True')

                properties = ', '.join(properties)
                field_type = self.fields_map[field['type']]
                lines.append(
                    f'{self.indent}{field["name"]} = models.{field_type}({properties})'
                )

            lines.append(self.empty_line)
            lines.append(self.empty_line)

        return lines
    
    @write_decorator
    def write_serializers(self):
        models = ', '.join(self.schema.keys())
        models_import = f'from .models import {models}'
        lines = [
            'from rest_framework.serializers import ModelSerializer',
            models_import,
            self.empty_line,
            self.empty_line,
        ]

        for class_name, fields in self.schema.items():
            lines.append(f'class {class_name}Serializer(ModelSerializer):')
            lines.append(f'{self.indent}model = {class_name}')
            fields = ', '.join([f'"{f["name"]}"' for f in fields])
            lines.append(f'{self.indent}fields = ({fields})')
            lines.append(self.empty_line)
            lines.append(self.empty_line)
        
        return lines

    @write_decorator
    def write_views(self):
        models = self.schema.keys()
        serializers = ', '.join([f'{m}Serializer' for m in models])
        serializers_import = f'from .serializers import {serializers}'
        models = ', '.join(models)
        models_import = f'from .models import {models}'
        lines = [
            'from rest_framework.viewsets import ModelViewset',
            models_import,
            serializers_import,
            self.empty_line,
            self.empty_line,
        ]

        for class_name, fields in self.schema.items():
            lines.append(f'class {class_name}Viewset(ModelViewset):')
            lines.append(f'{self.indent}queryset = {class_name}.objects.all()')
            lines.append(f'{self.indent}serializer = {class_name}Serializer')
            lines.append(self.empty_line)
            lines.append(self.empty_line)
        
        return lines


def convert(filepath):
    reader = Reader(filepath)
    reader.read_classes()
    writer = Writer(reader.classes)
    writer.write_models()
    writer.write_serializers()
    writer.write_views()


def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-fp", "--filepath", type=str, help="Path to uml file."
    )
    args = parser.parse_args()
    filepath = args.filepath
    if not filepath:
        raise argparse.ArgumentError(None, 'Filepath is required')

    convert(filepath)


if __name__ == '__main__':
    main()
