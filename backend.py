import json
import os
import pathlib

from aiohttp import web

import plantuml

from crud_generator_django import Reader, Writer


async def generate(request):
    data = await request.read()
    data = data.decode()
    reader = Reader(data)
    reader.read_classes()
    writer = Writer(reader.classes)
    writer.write_models()
    writer.write_serializers()
    writer.write_views()
    return web.Response(text='ok')


async def render(request):
    data = await request.read()
    data = data.decode()
    render = plantuml.PlantUML()
    result = render.processes(data)
    try:
        os.mkdir(pathlib.Path.cwd() / 'app')
    except FileExistsError:
        pass

    with open(pathlib.Path.cwd() / 'app' / 'output.png', 'wb') as output:
        output.write(result)

    return web.Response(text='ok')


app = web.Application()
app.add_routes([
    web.post('/generate', generate),
    web.post('/render', render)
])

web.run_app(app)
