# PlantUML to django converter

## How to use
1. `pipenv install` or use `pip install -r requirements.txt`
2. `pipenv run python backend.py` or `python backend.py`
3. Send response from `examples/renderder.sh`. Will be created `app` folder.
4. Find `output.png` in `app` folder.
3. Send response from `examples/generate.sh`.
4. Find `models.py`, `serializers.py`, `views.py` in `backend_template/schema` folder.
5. Go to `backend_template` and run `docker-compose up --build -d`

## PlantUML agreegments.

### Names
1. UO - User objects
2. Class - UO in PlantUML notation
3. Model - UO in Django notation
4. Serializer - UO in client frendly notation
5. Views - user interface to UO

### Class
1. Field must starts from `+` or `-` symbols. where `+` is required, `-` is not required field.
2. Fieldname should be without special carecters.
3. Awaliable types: Char, ForeignKey.
4. Field attributes divided from fieldname by `:` symbol.
5. Field attributes divided by space ` ` symbol.

## TODO:
1. Add web client interface instead Chrome Rester plugin and etc.
2. Library `cookiecutter` may be very useful.


# PlantUML to Django RestFramework convertor. DEPRECATED DESCRIPTION.

Convert `PlantUML` database description to `Django` CRUD application.

## Usage

```
python3 converter.py --filepath /path/to/database/diagram.pulm
```
