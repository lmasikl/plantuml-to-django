from django.db import models


class Device(models.Model):
    title = models.CharField(max_length=128)
    owner = models.ForeignKey(User)
    description = models.CharField(max_length=1024, blank=True, null=True)


class User(models.Model):
    name = models.CharField(max_length=64)
