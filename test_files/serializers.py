from rest_framework.serializers import ModelSerializer
from .models import Device, User


class DeviceSerializer(ModelSerializer):
    model = Device
    fields = ("id", "title", "owner", "description")


class UserSerializer(ModelSerializer):
    model = User
    fields = ("id", "name")
