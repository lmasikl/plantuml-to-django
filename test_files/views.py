from rest_framework.viewsets import ModelViewset
from .models import Device, User
from .serializers import DeviceSerializer, UserSerializer


class DeviceViewset(ModelViewset):
    queryset = Device.objects.all()
    serializer = DeviceSerializer


class UserViewset(ModelViewset):
    queryset = User.objects.all()
    serializer = UserSerializer
