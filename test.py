import unittest
from converter import convert
import os
from hashlib import md5

class ConvertorTestCase(unittest.TestCase):
    test_files = ['models.py', 'serializers.py', 'views.py']

    @classmethod
    def tearDown(cls):
        for test_file in cls.test_files:
            os.remove(test_file)

    def test_convertor(self):
        test_folder = './test_files/'
        filepath = f'{test_folder}products.pulm'
        convert(filepath)
        for file_name in self.test_files:
            with open(f'{test_folder}{file_name}', 'r') as sample_file:
                sample = sample_file.readlines()
            
            with open(f'{file_name}', 'r') as testing_file:
                testing = testing_file.readlines()
            
            for i, line in enumerate(testing):
                assert line == sample[i], (file_name, i)


if __name__ == '__main__':
    unittest.main()