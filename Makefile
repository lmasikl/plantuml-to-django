PYTHON = $(/usr/bin/env pipenv --py)
CURRENT_DIR = $(shell pwd)

default: run

install:
	@echo Install dependicices ...
	@pipenv install

run:
	@pipenv run python $(CURRENT_DIR)/backend.py

.PHONY: run default install
