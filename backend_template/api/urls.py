from django.conf.urls import url, include
from django.conf import settings
from django.conf.urls.static import static

from rest_framework.routers import DefaultRouter



router = DefaultRouter()
# TODO: implement generation
from schema.views import DeviceViewSet, UserViewSet
router.register(r'devices', DeviceViewSet)
router.register(r'users', UserViewSet)

urlpatterns = [
    url(r'^api/', include((router.urls, 'api'), namespace='api')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
